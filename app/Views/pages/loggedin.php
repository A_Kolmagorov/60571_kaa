<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<?= $this->section('content') ?>
    <div class="container d-flex justify-content-center" style="flex-direction: column;">
        <?php if (!empty($auto) and !empty($auto_id) and is_array($auto)) : ?>
        <div class="d-flex justify-content-center" style="flex-direction:column;">
            <div style="text-align:center">
                <h1 class="header__title">
                   Расходы на автомобили
            </div>
            <div class="d-flex justify-content-center pb-2">
                <label for="auto_id">Автомобиль:</label>
                <div>
                    <?= form_open('pages/view', ['style' => 'display: flex']); ?>
                        <select class="form-control" name='auto_id'
                            onChange=""
                            id="auto_id"
                    >
                        <?php foreach ($auto as $item): ?>
                            <option value=<?= esc($item['id']); ?> <?php if($auto_id['id']==$item['id']) echo "selected"; ?>>
                                <?= esc($item['brand']); ?> <?= esc($item['model']); ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                    <input type="submit" name="submit" style="display: none;">
                    </form>
                </div>
            </div>
        </div>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js'></script>
        <script>
            //Расходы за 3 года
            $(document).ready(function() {
                var ctx = $("#chart-line1");
                var myLineChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ["Сервис", "Страхование", "Мойка", "Заправка","ТО"],
                        datasets: [{
                            data: [<?php echo($serviceMonth['detcost']+$serviceMonth['workcost']);?>, <?php echo($insuranceMonth['cost']+0);?>, <?php echo($carwashMonth['washCost']+0);?>, <?php echo($fuelMonth['sum']+0)?>,<?php echo($techdiagnMonth['cost']+0);?>],
                            backgroundColor: ["rgba(255, 0, 0, 0.5)", "rgba(100, 255, 0, 0.5)", "rgba(200, 50, 255, 0.5)", "rgba(0, 100, 255, 0.5)", "rgba(55, 0, 230, 0.5)"]
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'За последний месяц'
                        }
                    }
                });
            });
        </script>
        <script>
            //Расходы за год
            $(document).ready(function() {
                var ctx = $("#chart-line2");
                var myLineChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ["Сервис", "Страхование", "Мойка", "Заправка","ТО"],
                        datasets: [{
                            data: [<?php echo($serviceYear['detcost']+$serviceYear['workcost']);?>, <?php echo($insuranceYear['cost']+0);?>, <?php echo($carwashYear['washCost']+0);?>, <?php echo($fuelYear['sum']+0)?>,<?php echo($techdiagnYear['cost']+0);?>],
                            backgroundColor: ["rgba(255, 0, 0, 0.5)", "rgba(100, 255, 0, 0.5)", "rgba(200, 50, 255, 0.5)", "rgba(0, 100, 255, 0.5)", "rgba(55, 0, 230, 0.5)"]
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'За последний год'
                        }
                    }
                });
            });
        </script>
        <script>
            //Расходы за месяц
            $(document).ready(function() {
                var ctx = $("#chart-line3");
                var myLineChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ["Сервис", "Страхование", "Мойка", "Заправка","ТО"],
                        datasets: [{
                            data: [<?php echo($service3Year['detcost']+$service3Year['workcost']);?>, <?php echo($insurance3Year['cost']+0);?>, <?php echo($carwash3Year['washCost']+0);?>, <?php echo($fuel3Year['sum']+0)?>,<?php echo($techdiagn3Year['cost']+0);?>],
                            backgroundColor: ["rgba(255, 0, 0, 0.5)", "rgba(100, 255, 0, 0.5)", "rgba(200, 50, 255, 0.5)", "rgba(0, 100, 255, 0.5)", "rgba(55, 0, 230, 0.5)"]
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'За последние 3 года'
                        }
                    }
                });
            });
        </script>
        <div class="card-deck">
        <div class="card">
            <div class="card-body" style="height:auto">
                <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                    <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                        <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                    </div>
                    <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                        <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                    </div>
                </div> <canvas id="chart-line1" width="299" height="200" class="chartjs-render-monitor" style=""></canvas>
            </div>
        </div>
            <div class="card">
                <div class="card-body" style="height:auto">
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                        </div>
                    </div> <canvas id="chart-line2" width="299" height="200" class="chartjs-render-monitor" style=""></canvas>
                </div>
            </div>
            <div class="card">
                <div class="card-body" style="height:auto">
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                        </div>
                    </div> <canvas id="chart-line3" width="299" height="200" class="chartjs-render-monitor" style=""></canvas>
                </div>
            </div>
        </div>
        <?php else : ?>
            <div class="text-center">
                <p>Нет добавленных автомобилей! </p>
                <a class="btn btn-outline-danger btn-lg" href="<?= base_url() ?>/auto/create">
                    Добавить автомобиль
                </a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>