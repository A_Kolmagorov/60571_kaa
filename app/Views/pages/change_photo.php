<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<?php use Config\Services;
use IonAuth\Libraries\IonAuth;
$ionAuth = new IonAuth();
helper(['form', 'url']);
$validation = Services::validation();
?>
<div class="text-center mr-3">
    <?php $hiddenFields = array('id' => $ionAuth->user()->row()->id); ?>
    <?= form_open_multipart('account/setAvatar', '', $hiddenFields); ?>
    <div class="form-group mt-3 mb-4">
        <h3>Изменить фото профиля:</h3>
        <input type="file" id="avatar"
               class="inputfile <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
               name="picture_url">
        <label class="btn btn-filter mb-0 pl-0" for="avatar">
            <span class="iconify" data-icon="entypo:upload" data-inline="false"></span>
            Нажмите для выбора
        </label>
        <button type="submit" class="btn btn-outline-primary d-none" name="btn-change-avatar">Применить</button>
        <div class="invalid-feedback">
            <?= $validation->getError('picture_url') ?>
        </div>
    </div>
    </form>
</div>
<?= $this->endSection() ?>