<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Автомобильные страхования:</h2>
        <div class="d-flex mb-3 justify-content-between">
            <?= $pager->links('group1', "my_page") ?>
            <?= form_open('insurance', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit">ОК</button>
            </form>
            <div class="d-flex justify-content-end">
                <?= form_open('insurance', ['style' => 'display: flex']); ?>
                <input type="text" class="form-control float-right" name="search" placeholder="Поиск по модели авто..."
                       aria-label="Search"
                value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit">Найти</button>
                </form>
            </div>
        </div>
        <div class="plushkiStyle mb-3">
            <button type="button" class="btn btn-primary" disabled="disabled">За последние 30 дней потрачено:<?= esc($insuranceSum['cost']); ?>₽</button>
            <button type="button" class="btn btn-primary" disabled="disabled">Страхований за последние 30 дней:<?= esc($insuranceCount['cost']); ?></button>
            <button type="button" class="btn btn-primary" disabled="disabled">В среднем потрачено:<?php if($insuranceCount['cost']==0) echo "0"; else{ echo ($insuranceSum['cost']/$insuranceCount['cost']);}?> ₽</button>
        </div>
        <?php if (!empty($insurance) && is_array($insurance)) : ?>
            <table class="resp-tab mb-2 text-center">
                <thead>
                <tr>
                    <th>Тип страхования</th>
                    <th>Ст-сть страхования</th>
                    <th>Автомобиль</th>
                    <th>Дата</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($insurance as $item): ?>
                    <tr>
                        <td><span>Тип страхования</span><?= esc($item['typeOfInsurance']); ?></td>
                        <td><span>Ст-сть страхования</span><?= esc($item['cost']); ?> ₽</td>
                        <td><span>Автомобиль</span><?= esc($item['brand']); ?> <?= esc($item['model']); ?></td>
                        <td><span>Дата</span><?= esc($item['insuranceDate']); ?></td>
                        <td><span>Действия</span>
                            <div class="row d-flex justify-content-center">
                                <div class="mr-1">
                                    <a href="<?= base_url() ?>/insurance/view/<?= esc($item['id']); ?>"
                                       class="btn btn-primary btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Подробнее">
                                        <span class="iconify" data-icon="bi:card-list" data-inline="false"></span>
                                    </a>
                                </div>
                                <div class="mr-1">
                                    <a href="<?= base_url() ?>/insurance/edit/<?= esc($item['id']); ?>"
                                       class="btn btn-warning btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Редактировать">
                                        <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span>
                                    </a>
                                </div>
                                <div>
                                    <a href="<?= base_url() ?>/insurance/delete/<?= esc($item['id']); ?>"
                                       class="btn btn-danger btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Удалить">
                                        <span class="iconify" data-icon="ant-design:delete-filled" data-inline="false"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="text-center">
                <a class="btn btn-outline-primary" href="<?= base_url() ?>/insurance/create">
                    Добавить запись
                </a>
            </div>
        <?php else : ?>
            <div class="text-center">
                <p>Записи не найдены </p>
                <a class="btn btn-outline-danger btn-lg" href="<?= base_url() ?>/insurance/create">
                    Создать запись
                </a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>