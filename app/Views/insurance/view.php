<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($insurance)) : ?>
        <div class="d-column-flex text-center py-3">
            <div>
                <?php if (is_null($insurance['picture_url'])) : ?>
                    <img src="https://granatgold.ru/local/templates/granatgold/components/bitrix/catalog.section/block/images/tile-empty.jpg"
                         class="mb-3" width="400px" alt="">
                <?php else: ?>
                    <!----modal starts here--->
                    <div id="tutorialsplaneModal" class="modal fade" role='dialog'>
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <img src="<?= esc($insurance['picture_url']); ?>" style="width:90%;height:90%;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Modal ends here--->

                    <a href="javascript:void(0);" class="" data-toggle="modal" data-target="#tutorialsplaneModal">
                        <img class="mb-3" src="<?= esc($insurance['picture_url']); ?>" width="350px"> <br>
                    </a>
                <?php endif ?>
            </div>
            <div>
                <div>Автомобиль: <?= esc($auto['brand']); ?> <?= esc($auto['model']); ?></div>
                <div>Тип страхования: <?= esc($insurance['typeOfInsurance']); ?></div>
                <div>Пробег: <?= esc($insurance['mileage']); ?> км.</div>
                <div>Стоимость страхования: <?= esc($insurance['cost']); ?> ₽</div>
                <div>Дата страхования: <?= esc($insurance['insuranceDate']); ?></div>
                <div>Срок страхования: <?= esc($insurance['insuranceDuration']); ?></div>
                <div>Страхования компания: <?= esc($insurance['company']); ?></div>
            </div>
        </div>
        <div class="text-center">
            <a class="btn btn-outline-primary btn-lg" href="<?= base_url() ?>/insurance">
                Назад
            </a>
        </div>
    <?php else : ?>
        <p>Запись не найдена.</p>
        <div class="text-center">
            <a class="btn btn-outline-primary btn-lg" href="<?= base_url() ?>/insurance">
                Назад
            </a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
