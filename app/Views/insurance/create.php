<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('insurance/store'); ?>
    <h2>Добавление записи о страховании:</h2>
    <div class="form-group">
        <label for="auto_id">Автомобиль:</label>
        <select class="form-control <?= ($validation->hasError('auto_id')) ? 'is-invalid' : ''; ?>" name='auto_id'
                onChange=""
                id="auto_id"
        >
            <option value="">Выберите автомобиль</option>
            <?php foreach ($auto as $item): ?>
                <option value=<?= esc($item['id']); ?>>
                    <?= esc($item['brand']); ?> <?= esc($item['model']); ?>
                </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('auto_id') ?>
        </div>
    </div>
    <div class="form-group">
        <div class="form-group">
            <label for="typeOfInsurance">Тип страхования:</label>
            <select class="form-control <?= ($validation->hasError('typeOfInsurance')) ? 'is-invalid' : ''; ?>" name='typeOfInsurance' onChange="" id="typeOfInsurance">
                <option value="Не указано">Выберите тип страхования:</option>
                <option value='КАСКО'>КАСКО</option>
                <option value='ОСАГО'>ОСАГО</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('typeOfInsurance') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="company">Страховая компания:</label>
            <input type="text" class="form-control <?= ($validation->hasError('company')) ? 'is-invalid' : ''; ?>"
                   name="company"
                   id="company"
                   value="<?= old('company'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('company') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="insuranceDuration">Срок страхования(мес.):</label>
            <input type="number" min="1" step="1" class="form-control <?= ($validation->hasError('insuranceDuration')) ? 'is-invalid' : ''; ?>"
                   name="insuranceDuration"
                   id="insuranceDuration"
                   value="<?= old('insuranceDuration'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('insuranceDuration') ?>
            </div>
        </div>
        <label for="mileage">Пробег:</label>
        <input type="number" min="1" step="1" class="form-control <?= ($validation->hasError('mileage')) ? 'is-invalid' : ''; ?>"
               name="mileage"
               id="mileage"
               value="<?= old('mileage'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('mileage') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="cost">Стоимость страхования:</label>
        <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('cost')) ? 'is-invalid' : ''; ?>"
               name="cost"
               id="cost"
               value="<?= old('cost'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('cost') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="insuranceDate">Дата оформления:</label>
        <input type="date" class="form-control <?= ($validation->hasError('insuranceDate')) ? 'is-invalid' : ''; ?>"
               name="insuranceDate"
               id="insuranceDate"
               value="<?= old('insuranceDate'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('insuranceDate') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="picture_url">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
               name="picture_url" id="picture_url">
        <div class="invalid-feedback">
            <?= $validation->getError('picture_url') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Добавить</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
