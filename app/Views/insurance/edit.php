<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('insurance/update'); ?>
        <h2>Редактирование данных о страховании:</h2>
        <input type="hidden" name="id" value="<?= $insurance["id"] ?>">
        <div class="form-group">
            <div class="form-group">
                <label for="company">Страховая компания:</label>
                <input type="text" class="form-control <?= ($validation->hasError('company')) ? 'is-invalid' : ''; ?>"
                       name="company"
                       id="company"
                       value="<?= $insurance["company"]; ?>">
                <div class="invalid-feedback">
                    <?= $validation->getError('company') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="insuranceDuration">Срок страхования(мес.):</label>
                <input type="number" min="1" step="1" class="form-control <?= ($validation->hasError('insuranceDuration')) ? 'is-invalid' : ''; ?>"
                       name="insuranceDuration"
                       id="insuranceDuration"
                       value="<?= $insurance["insuranceDuration"]; ?>">
                <div class="invalid-feedback">
                    <?= $validation->getError('insuranceDuration') ?>
                </div>
            </div>
            <label for="mileage">Пробег:</label>
            <input type="number" min="1" step="1" class="form-control <?= ($validation->hasError('mileage')) ? 'is-invalid' : ''; ?>"
                   name="mileage"
                   id="mileage"
                   value="<?= $insurance["mileage"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('mileage') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="cost">Стоимость страхования:</label>
            <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('cost')) ? 'is-invalid' : ''; ?>"
                   name="cost"
                   id="cost"
                   value="<?= $insurance["cost"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('cost') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="insuranceDate">Дата оформления:</label>
            <input type="date" class="form-control <?= ($validation->hasError('insuranceDate')) ? 'is-invalid' : ''; ?>"
                   name="insuranceDate"
                   id="insuranceDate"
                   value="<?= $insurance["insuranceDate"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('insuranceDate') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="picture_url">Изображение:</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
                   name="picture_url" id="picture_url">
            <div class="invalid-feedback">
                <?= $validation->getError('picture_url') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
<?php
