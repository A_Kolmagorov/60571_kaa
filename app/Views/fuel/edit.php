<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('fuel/update'); ?>
        <h2>Редактирование записи о заправке:</h2>
        <input type="hidden" name="id" value="<?= $fuel["id"] ?>">
        <div class="form-group">
            <label for="liters">Кол-во литров:</label>
            <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('liters')) ? 'is-invalid' : ''; ?>"
                   name="liters"
                   id="liters"
                   value="<?=$fuel['liters']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('liters') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="literCost">Цена за литр:</label>
            <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('literCost')) ? 'is-invalid' : ''; ?>"
                   name="literCost"
                   id="literCost"
                   value="<?= $fuel['literCost']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('literCost') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="date">Дата заправки:</label>
            <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
                   name="date"
                   id="date"
                   value="<?= $fuel["date"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
<?php
