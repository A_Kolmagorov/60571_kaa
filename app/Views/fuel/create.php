<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('fuel/store'); ?>
    <h2>Добавление записи о заправке:</h2>
    <div class="form-group">
        <label for="auto_id">Авто:</label>
        <select class="form-control <?= ($validation->hasError('auto_id')) ? 'is-invalid' : ''; ?>" name='auto_id'
                onChange=""
                id="auto_id"
        >
            <option value="">Выберите авто</option>
            <?php foreach ($auto as $item): ?>
                <option value=<?= esc($item['id']); ?>>
                    <?= esc($item['brand']); ?> <?= esc($item['model']); ?>
                </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('auto_id') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="fuelType">Вид топлива:</label>
        <select class="form-control <?= ($validation->hasError('fuelType')) ? 'is-invalid' : ''; ?>" name='fuelType' onChange="" id="fuelType">
            <option value="Не указано">Выберите топливо</option>
            <option value='92'>92</option>
            <option value='95'>95</option>
            <option value='98'>98</option>
            <option value='100'>100</option>
            <option value='ДТ'>ДТ</option>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('fuelType') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="liters">Заправлено литров:</label>
        <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('liters')) ? 'is-invalid' : ''; ?>"
               name="liters"
               id="liters"
               value="<?= old('liters'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('liters') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="literCost">Цена за литр:</label>
        <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('literCost')) ? 'is-invalid' : ''; ?>"
               name="literCost"
               id="literCost"
               value="<?= old('literCost'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('literCost') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="date">Дата заправки:</label>
        <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
               name="date"
               id="date"
               value="<?= old('date'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('date') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Добавить</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
