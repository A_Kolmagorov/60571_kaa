<!DOCTYPE html>
<head>
    <title>Бортовой журнал автомобиля</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"><script src="https://kit.fontawesome.com/6e9b058a28.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="/styles/style.css"/>
</head>
<body>
<nav class="navbar navbar-expand-lg">
    <div class="container">
            <a class="navbar-brand" href="<?= base_url() ?>"> <img src="/images/logo.jpg" alt=""></a>
            <button class="navbar-toggler third-button ms-auto" type="button" data-toggle="collapse"
                    data-target="#navbarsExampleDefault"
                    aria-controls="navbarsExampleDefault" aria-expanded="false"
                    aria-label="Toggle navigation">
                <div class="animated-icon3"><span></span><span></span><span></span></div>
            </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav w-100 align-items-center">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>/auto"
                       aria-haspopup="true" aria-expanded="true">Автомобиль
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>/service"
                       aria-haspopup="true" aria-expanded="true">Сервис
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>/insurance"
                       aria-haspopup="true" aria-expanded="true">Страхование
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>/carwash"
                       aria-haspopup="true" aria-expanded="true">Мойка
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>/fuel"
                       aria-haspopup="true" aria-expanded="true">Заправка
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>/techdiagn"
                       aria-haspopup="true" aria-expanded="true">Тех. осмотр
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>/notes"
                       aria-haspopup="true" aria-expanded="true">Заметки
                    </a>
                </li>
                <?php use IonAuth\Libraries\IonAuth;
                $ionAuth = new IonAuth();
                helper(['form', 'url']);
                $validation = \Config\Services::validation();
                ?>
                <?php if (!$ionAuth->loggedIn()): ?>
                        <li class="nav-item ml-auto registrationStyle">
                        <a href="<?= base_url() ?>/auth/register_user" class="nav-link button button_outline" role="button">Зарегистрироваться</a>
                    </li>
                <?php endif ?>
                <?php if (!$ionAuth->loggedIn()): ?>
                    <li class="nav-item ml-1 mr-0">
                        <a class="nav-link  button button_primary" href="<?= base_url() ?>/auth/login">Войти</a>
                    </li>
                <?php else: ?>
                    <li class="account__info ml-auto mr-0 d-flex align-items-center marginleft0">
                        <!--
                        <div class="mr-3">
                            <?php $hiddenFields = array('id' => $ionAuth->user()->row()->id); ?>
                            <?= form_open_multipart('account/setAvatar', '', $hiddenFields); ?>
                            <div class="form-group mt-3 mb-4">
                                <input type="file" id="avatar"
                                       class="inputfile <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
                                       name="picture_url">
                                <label class="btn btn-filter mb-0 pl-0" for="avatar">
                                    <span class="iconify" data-icon="entypo:upload" data-inline="false"></span>
                                    Изменить аватар
                                </label>
                                <button type="submit" class="btn btn-outline-primary d-none" name="btn-change-avatar">Применить
                                </button>
                                <div class="invalid-feedback">
                                    <?= $validation->getError('picture_url') ?>
                                </div>
                            </div>
                            </form>
                        </div>
                        -->
                        <div class="nav-item">
                            <div class="mr-3 d-flex align-items-center loginStyle">
                                <?php if ($ionAuth->user()->row()->picture_url !== null): ?>
                                    <div class="client-item__img mr-3 photoStyle">
                                        <a href="<?php base_url()?>/pages/view/change_photo">
                                            <img src="<?php echo $ionAuth->user()->row()->picture_url; ?>"
                                            style="width: 55px; height: 55px; border-radius: 50%;" alt="Фото">
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <div class="client-item__img mr-3">
                                        <a href="<?php base_url()?>/pages/view/change_photo">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png"
                                            style="width: 55px; height: 55px; border-radius: 50%;" alt="Фото">
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <div>
                                    <div class="client-item__name" style="margin-left: 10px;">
                                        <?php echo $ionAuth->user()->row()->first_name; ?>
                                        <?php echo $ionAuth->user()->row()->last_name; ?></div>
                                    <div class="client-item__mail"><?php echo $ionAuth->user()->row()->email; ?></div>
                                </div>
                                <a class="nav-link button button_primary" href="<?= base_url() ?>/auth/logout">Выйти</a>
                            </div>
                        </div>
                    </li>
                <?php endif ?>
            </ul>
        </div>
    </div>

</nav>
<hr class="hrStyle" style="display:none">
<?php if (session()->getFlashdata('message')) : ?>
    <div class="container text-center d-flex justify-content-center">
        <div class="alert alert-info d-flex justify-content-center" role="alert" style="max-width: 540px;">
            <?= session()->getFlashdata('message') ?>
        </div>
    </div>
<?php endif ?>

<?= $this->renderSection('content') ?>
<div style="padding-top: 150px !important;">&nbsp</div>

</main>
<footer class="text-center text-white fixed-bottom mt-auto" style="background-color: #f1f1f1;">
<!-- Grid container -->
<div class="container pt-3 footerpaddingtop">
    <!-- Section: Social media -->
    <section class="mb-3 footermarginbottom">
        <!-- Facebook -->
        <a
                class="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark"
        ><i class="fab fa-facebook-f"></i
            ></a>

        <!-- Twitter -->
        <a
                class="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark"
        ><i class="fab fa-twitter"></i
            ></a>

        <!-- Google -->
        <a
                class="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark"
        ><i class="fab fa-google"></i
            ></a>

        <!-- Instagram -->
        <a
                class="btn btn-link btn-floating btn-lg text-dark m-1"
                href="#!"
                role="button"
                data-mdb-ripple-color="dark"
        ><i class="fab fa-instagram"></i
            ></a>
    </section>
    <!-- Section: Social media -->
</div>
<!-- Grid container -->

<!-- Copyright -->
<div class="text-center text-dark p-3 footerend" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2021 Все права защищены -
    <a class="text-dark" href="mailto:Kolmagorov.surgut@yandex.ru">Колмагоров Александр</a>
</div>
<!-- Copyright -->
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
<script src="/app.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>