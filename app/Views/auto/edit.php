<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('auto/update'); ?>
        <h2>Редактирование параметров автомобиля:</h2>
        <input type="hidden" name="id" value="<?= $auto["id"] ?>">
        <div class="form-group">
            <label for="brand">Бренд:</label>
            <input type="text" class="form-control <?= ($validation->hasError('brand')) ? 'is-invalid' : ''; ?>"
                   name="brand"
                   id="brand"
                   value="<?=$auto['brand']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('brand') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="model">Модель:</label>
            <input type="text" class="form-control <?= ($validation->hasError('model')) ? 'is-invalid' : ''; ?>"
                   name="model"
                   id="model"
                   value="<?= $auto['model']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('model') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="year">Год:</label>
            <input type="number" min="1980" step="1" class="form-control <?= ($validation->hasError('year')) ? 'is-invalid' : ''; ?>"
                   name="year"
                   id="year"
                   value="<?= $auto['year']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('year') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="fuel">Вид топлива:</label>
            <select class="form-control <?= ($validation->hasError('fuel')) ? 'is-invalid' : ''; ?>" name='fuel' onChange="" id="fuel">
                <option value="Не указано">Выберите топливо</option>
                <option value='92' <?php if ($auto['fuel'] == '92') echo("selected"); ?>>92</option>
                <option value='95' <?php if ($auto['fuel'] == '95') echo("selected"); ?>>95</option>
                <option value='98' <?php if ($auto['fuel'] == '98') echo("selected"); ?>>98</option>
                <option value='100' <?php if ($auto['fuel'] == '100') echo("selected"); ?>>100</option>
                <option value='ДТ'  <?php if ($auto['fuel'] == 'ДТ') echo("selected"); ?>>ДТ</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('fuel') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="picture">Изображение:</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>"
                   name="picture" id="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
<?php
