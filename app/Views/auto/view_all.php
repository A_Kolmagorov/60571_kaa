<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <h2>Автомобили:</h2>
    <div class="d-flex justify-content-between mb-4">
        <?= $pager->links('group1', "my_page") ?>
        <?= form_open('auto', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit">На странице</button>
        </form>
        <?= form_open('auto', ['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Поиск по модели авто..."
               aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit">Найти</button>
        </form>
    </div>
    <?php if (!empty($auto) && is_array($auto)) : ?>
        <table class="resp-tab mb-2 text-center">
            <thead>
            <tr>
                <th>Фото</th>
                <th>Бренд</th>
                <th>Модель</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($auto as $item): ?>
                <tr>
                    <td><span>Фото</span>
                        <div>
                            <?php if (is_null($item['picture_url'])) : ?>
                                <img src="https://granatgold.ru/local/templates/granatgold/components/bitrix/catalog.section/block/images/tile-empty.jpg"
                                     alt="" width="250px">
                            <?php else:?>
                                <div id="tutorialsplaneModal" class="modal fade" role='dialog'>
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="<?= esc($item['picture_url']); ?>" style="width:90%;height:90%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#tutorialsplaneModal">
                                    <img src="<?= esc($item['picture_url']); ?>" width="300px"> <br>
                                </a>
                            <?php endif ?>
                        </div>
                    </td>
                    <td><span>Бренд</span><?= esc($item['brand']); ?></td>
                    <td><span>Модель</span><?= esc($item['model']); ?></td>
                    <td><span>Действия</span>
                        <div class="row d-flex justify-content-center">
                            <div class="mr-1">
                                <a href="<?= base_url() ?>/auto/view/<?= esc($item['id']); ?>"
                                   class="btn btn-primary btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Подробнее">
                                    <span class="iconify" data-icon="bi:card-list" data-inline="false"></span>
                                </a>
                            </div>
                            <div class="mr-1">
                                <a href="<?= base_url() ?>/auto/edit/<?= esc($item['id']); ?>"
                                   class="btn btn-warning btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Редактировать">
                                    <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span>
                                </a>
                            </div>
                            <div>
                                <a href="<?= base_url() ?>/auto/delete/<?= esc($item['id']); ?>"
                                   class="btn btn-danger btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Удалить">
                                    <span class="iconify" data-icon="ant-design:delete-filled" data-inline="false"></span>
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="text-center">
            <a class="btn btn-outline-primary" href="<?= base_url() ?>/auto/create">
                Добавить автомобиль
            </a>
        </div>
    <?php else : ?>
        <div class="text-center">
            <p>Записи не найдены </p>
            <a class="btn btn-primary btn-lg" href="<?= base_url() ?>/auto/create">
                Добавить автомобиль
            </a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
