<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('auto/store'); ?>
    <div class="form-group">
        <label for="brand">Бренд:</label>
        <input type="text" class="form-control <?= ($validation->hasError('brand')) ? 'is-invalid' : ''; ?>"
               name="brand"
               id="brand"
               value="<?= old('brand'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('brand') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="model">Модель:</label>
        <input type="text" class="form-control <?= ($validation->hasError('model')) ? 'is-invalid' : ''; ?>"
               name="model"
               id="model"
               value="<?= old('model'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('model') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="year">Год:</label>
        <input type="number" min="1980" step="1" class="form-control <?= ($validation->hasError('year')) ? 'is-invalid' : ''; ?>"
               name="year"
               id="year"
               value="<?= old('year'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('year') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="fuel">Вид топлива:</label>
        <select class="form-control <?= ($validation->hasError('fuel')) ? 'is-invalid' : ''; ?>" name='fuel' onChange="" id="fuel">
            <option value="Не указано">Выберите топливо</option>
            <option value='92'>92</option>
            <option value='95'>95</option>
            <option value='98'>98</option>
            <option value='100'>100</option>
            <option value='ДТ'>ДТ</option>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('fuel') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="picture">Изображение:</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>"
               name="picture" id="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
