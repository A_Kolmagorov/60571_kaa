<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <h2>Сервис:</h2>
    <div class="d-flex mb-3 justify-content-between">
        <?= $pager->links('group1', "my_page") ?>
        <?= form_open('service', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit">ОК</button>
        </form>
        <div class="d-flex justify-content-end">
            <?= form_open('service', ['style' => 'display: flex']); ?>
            <input type="text" class="form-control float-right" name="search" placeholder="Поиск по типу работ..."
                   aria-label="Search"
            value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit">Найти</button>
            </form>
        </div>
    </div>
    <div class="plushkiStyle mb-3">
        <button type="button" class="btn btn-primary" disabled="disabled">За последние 30 дней потрачено:<?= esc($serviceSum['workcost'])+esc($serviceSum['detcost']); ?>₽</button>
        <button type="button" class="btn btn-primary" disabled="disabled">Обслуживаний за последние 30 дней:<?= esc($serviceCount['workcost']); ?></button>
        <button type="button" class="btn btn-primary" disabled="disabled">В среднем потрачено:<?php if($serviceCount['workcost']==0) echo "0"; else{ echo (($serviceSum['workcost']+$serviceSum['detcost'])/$serviceCount['workcost']);}?> ₽</button>
    </div>
    <?php if (!empty($service) && is_array($service)) : ?>
        <table class="resp-tab mb-2 text-center">
            <thead>
            <tr>
                <th>Тип работы</th>
                <th>Ст-сть работы</th>
                <th>Ст-сть запчасти</th>
                <th>Сервис</th>
                <th>Автомобиль</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($service as $item): ?>
                <tr>
                    <td><span>Тип работы</span><?= esc($item['typeofwork']); ?></td>
                    <td><span>Ст-сть работы</span><?= esc($item['workcost']); ?> ₽</td>
                    <td><span>Ст-сть запчасти</span><?= esc($item['detcost']); ?> ₽</td>
                    <td><span>Сервис</span><?= esc($item['servicename']); ?></td>
                    <td><span>Автомобиль</span><?= esc($item['brand']); ?> <?= esc($item['model']); ?></td>
                    <td><span>Действия</span>
                        <div class="row d-flex justify-content-center">
                            <div class="mr-1">
                                <a href="<?= base_url() ?>/service/view/<?= esc($item['id']); ?>"
                                   class="btn btn-primary btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Подробнее">
                                    <span class="iconify" data-icon="bi:card-list" data-inline="false"></span>
                                </a>
                            </div>
                            <div class="mr-1">
                                <a href="<?= base_url() ?>/service/edit/<?= esc($item['id']); ?>"
                                   class="btn btn-warning btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Редактировать">
                                    <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span>
                                </a>
                            </div>
                            <div>
                                <a href="<?= base_url() ?>/service/delete/<?= esc($item['id']); ?>"
                                   class="btn btn-danger btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Удалить">
                                    <span class="iconify" data-icon="ant-design:delete-filled" data-inline="false"></span>
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="text-center">
            <a class="btn btn-outline-primary" href="<?= base_url() ?>/service/create">
                Добавить запись
            </a>
        </div>
    <?php else : ?>
        <div class="text-center">
            <p>Записи не найдены </p>
            <a class="btn btn-outline-danger btn-lg" href="<?= base_url() ?>/service/create">
                Создать запись
            </a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
