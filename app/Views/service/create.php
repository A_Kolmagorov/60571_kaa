<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('service/store'); ?>
    <h2>Добавление записи о сервисе:</h2>
    <div class="form-group">
        <label for="date">Дата:</label>
        <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
               name="date"
               id="date"
               value="<?= old('date'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('date') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="detcost">Стоимость детали:</label>
        <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('detcost')) ? 'is-invalid' : ''; ?>"
               name="detcost"
               id="detcost"
               value="<?= old('detcost'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('detcost') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="typeofwork">Тип работы:</label>
        <input type="text" class="form-control <?= ($validation->hasError('typeofwork')) ? 'is-invalid' : ''; ?>"
               name="typeofwork"
               id="typeofwork"
               value="<?= old('typeofwork'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('typeofwork') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="workcost">Стоимость работы:</label>
        <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('workcost')) ? 'is-invalid' : ''; ?>"
               name="workcost"
               id="workcost"
               value="<?= old('workcost'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('workcost') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="servicename">Название сервиса:</label>
        <input type="text" class="form-control <?= ($validation->hasError('servicename')) ? 'is-invalid' : ''; ?>"
               name="servicename"
               id="servicename"
               value="<?= old('servicename'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('servicename') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="mileage">Пробег:</label>
        <input type="number" min="1" step="1" class="form-control <?= ($validation->hasError('mileage')) ? 'is-invalid' : ''; ?>"
               name="mileage"
               id="mileage"
               value="<?= old('mileage'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('mileage') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="auto_id">Авто</label>
        <select class="form-control <?= ($validation->hasError('auto_id')) ? 'is-invalid' : ''; ?>" name='auto_id'
                onChange=""
                id="auto_id"
        >
            <option value="">Выберите авто</option>
            <?php foreach ($auto as $item): ?>
                <option value=<?= esc($item['id']); ?>>
                    <?= esc($item['brand']); ?> <?= esc($item['model']); ?>
                </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('auto_id') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="picture_url">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
               name="picture_url" id="picture_url">
        <div class="invalid-feedback">
            <?= $validation->getError('picture_url') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
