<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('service/update'); ?>
        <h2>Редактирование записи о сервисе:</h2>
        <input type="hidden" name="id" value="<?= esc($service['id']) ?>">
        <div class="form-group">
            <label for="name">Дата:</label>
            <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
                   name="date"
                   id="name"
                   value="<?= $service['date']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="detcost">Стоимость детали:</label>
            <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('detcost')) ? 'is-invalid' : ''; ?>"
                   name="detcost"
                   id="detcost"
                   value="<?= $service['detcost']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('detcost') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="typeofwork">Тип работы:</label>
            <input type="text" class="form-control <?= ($validation->hasError('typeofwork')) ? 'is-invalid' : ''; ?>"
                   name="typeofwork"
                   id="typeofwork"
                   value="<?= $service['typeofwork']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('typeofwork') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="workcost">Стоимость работы:</label>
            <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('workcost')) ? 'is-invalid' : ''; ?>"
                   name="workcost"
                   id="workcost"
                   value="<?= $service['workcost']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('workcost') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="servicename">Название сервиса:</label>
            <input type="text" class="form-control <?= ($validation->hasError('servicename')) ? 'is-invalid' : ''; ?>"
                   name="servicename"
                   id="servicename"
                   value="<?= $service['servicename']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('servicename') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="mileage">Пробег:</label>
            <input type="number" min="1" step="1" class="form-control <?= ($validation->hasError('mileage')) ? 'is-invalid' : ''; ?>"
                   name="mileage"
                   id="mileage"
                   value="<?= $service['mileage']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('mileage') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="picture_url">Изображение:</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
                   name="picture_url" id="picture_url">
            <div class="invalid-feedback">
                <?= $validation->getError('picture_url') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
<?php
