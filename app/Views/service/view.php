<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($service)) : ?>
        <div class="d-column-flex text-center py-3">
            <div>
                <?php if (is_null($service['picture_url'])) : ?>
                    <img src="https://granatgold.ru/local/templates/granatgold/components/bitrix/catalog.section/block/images/tile-empty.jpg"
                         class="mb-3" width="400px" alt="">
                <?php else: ?>
                    <!----modal starts here--->
                    <div id="tutorialsplaneModal" class="modal fade" role='dialog'>
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <img src="<?= esc($service['picture_url']); ?>" style="width:90%;height:90%;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Modal ends here--->

                    <a href="javascript:void(0);" class="" data-toggle="modal" data-target="#tutorialsplaneModal">
                        <img class="mb-3" src="<?= esc($service['picture_url']); ?>" width="350px"> <br>
                    </a>
                <?php endif ?>
            </div>
            <div>
                <div>Автомобиль: <?= esc($auto['brand']); ?> <?= esc($auto['model']); ?></div>
                <div>Тип работы: <?= esc($service['typeofwork']); ?></div>
                <div>Название сервиса: <?= esc($service['servicename']); ?></div>
                <div>Стоимость работы: <?= esc($service['workcost']); ?> ₽</div>
                <div>Стоимость деталей: <?= esc($service['detcost']); ?> ₽</div>
                <div>Пробег: <?= esc($service['mileage']); ?> км.</div>
                <div>Дата: <?= esc($service['date']); ?></div>
            </div>
        </div>
        <div class="text-center">
            <a class="btn btn-outline-primary btn-lg" href="<?= base_url() ?>/service">
                Назад
            </a>
        </div>
    <?php else : ?>
        <p>Запись не найдена.</p>
        <div class="text-center">
            <a class="btn btn-outline-primary btn-lg" href="<?= base_url() ?>/service">
                Назад
            </a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
