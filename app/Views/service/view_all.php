<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2 class="mb-3">Все записи</h2>
        <div class="row text-center mb-3">
            <div class="col-1">Фото</div>
            <div class="col-2">Сервис</div>
            <div class="col-2">Тип работы</div>
            <div class="offset-8"></div>
        </div>
        <?php if (!empty($car) && is_array($car)) : ?>
            <?php foreach ($car as $item): ?>
                <div class="row align-items-center text-center py-3 mb-3" style="border: 2px solid grey; border-radius: 10px;">
                    <div class="col-1">
                        <?php if (is_null($item['picture_url'])) : ?>
                            <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.ytimg.com%2Fvi%2FyjEeya5aaf8%2Fmaxresdefault.jpg&f=1&nofb=1"
                                 class="w-100" alt="">
                        <?php else:?>
                            <img src="<?= esc($item['picture_url']); ?>" class="w-100" alt="">
                        <?php endif ?>
                    </div>
                    <h5 class="col-2 m-0"><?= esc($item['servicename']); ?></h5>
                    <div class="col-2"><?= esc($item['typeofwork']); ?></div>
                    <div class="col-2">
                        <a href="<?= base_url() ?>/service/view/<?= esc($item['id']); ?>"
                                          class="btn btn-primary">Просмотреть</a>
                    </div>
                    <div class="col-3">
                        <a href="<?= base_url() ?>/service/edit/<?= esc($item['id']); ?>"
                                          class="btn btn-primary">Редактировать</a>
                    </div>
                    <div class="col-2">
                        <a href="<?= base_url() ?>/service/delete/<?= esc($item['id']); ?>"
                                          class="btn btn-primary">Удалить</a>
                    </div>

                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти рейтинги.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>