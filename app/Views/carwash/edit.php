<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('carwash/update'); ?>
        <h2>Редактирование записи о мойке:</h2>
        <input type="hidden" name="id" value="<?= $carwash["id"] ?>">
        <div class="form-group">
            <label for="washcompany">Название компании:</label>
            <input type="text" class="form-control <?= ($validation->hasError('washcompany')) ? 'is-invalid' : ''; ?>"
                   name="washcompany"
                   id="washcompany"
                   value="<?=$carwash['washcompany']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('washcompany') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="washCost">Стоимость мойки:</label>
            <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('washCost')) ? 'is-invalid' : ''; ?>"
                   name="washCost"
                   id="washCost"
                   value="<?=$carwash['washCost']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('washCost') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="washText">Отзыв:</label>
            <textarea rows="5" cols="60"  placeholder="Отзыв о мойке в данной компании..." class="form-control <?= ($validation->hasError('washText')) ? 'is-invalid' : ''; ?>"
                      name="washText"
                      id="washText"><?=$carwash['washText']; ?></textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('washText') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="date">Дата:</label>
            <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
                   name="date"
                   id="date"
                   value="<?= $carwash['date']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
<?php
