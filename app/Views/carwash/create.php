<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('carwash/store'); ?>
    <h2>Добавление записи о мойке:</h2>
    <div class="form-group">
        <label for="auto_id">Авто:</label>
        <select class="form-control <?= ($validation->hasError('auto_id')) ? 'is-invalid' : ''; ?>" name='auto_id'
                onChange=""
                id="auto_id"
        >
            <option value="">Выберите авто:</option>
            <?php foreach ($auto as $item): ?>
                <option value=<?= esc($item['id']); ?>>
                    <?= esc($item['brand']); ?> <?= esc($item['model']); ?>
                </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('auto_id') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="typeOfWash">Тип мойки:</label>
        <select class="form-control <?= ($validation->hasError('typeOfWash')) ? 'is-invalid' : ''; ?>" name='typeOfWash' onChange="" id="typeOfWash">
            <option value="Не указано">Выберите тип мойки:</option>
            <option value="Самообслуживание">Самообслуживание</option>
            <option value="Комплексная мойка">Комплексная мойка</option>
            <option value="Наружная мойка">Наружная мойка</option>
            <option value="Комплексная + воск">Комплексная + воск</option>
            <option value="Наружная + воск">Наружная + воск</option>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('typeOfWash') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="washCost">Стоимость мойки:</label>
        <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('washCost')) ? 'is-invalid' : ''; ?>"
               name="washCost"
               id="washCost"
               value="<?= old('washCost'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('washCost') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="washcompany">Название компании:</label>
        <input type="text" class="form-control <?= ($validation->hasError('washcompany')) ? 'is-invalid' : ''; ?>"
               name="washcompany"
               id="washcompany"
               value="<?= old('washcompany'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('washcompany') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="washText">Отзыв:</label>
        <textarea rows="5" cols="60"  placeholder="Отзыв о мойке в данной компании..." class="form-control <?= ($validation->hasError('washText')) ? 'is-invalid' : ''; ?>"
                  name="washText"
                  id="washText"></textarea>
        <div class="invalid-feedback">
            <?= $validation->getError('washText') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="year">Дата:</label>
        <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
               name="date"
               id="date"
               value="<?= old('date'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('date') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Добавить</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
