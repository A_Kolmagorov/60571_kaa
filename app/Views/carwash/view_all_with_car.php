<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Автомойка:</h2>
        <div class="d-flex mb-3 justify-content-between">
            <?= $pager->links('group1', "my_page") ?>
            <?= form_open('carwash', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit">ОК</button>
            </form>
            <div class="d-flex justify-content-end">
                <?= form_open('carwash', ['style' => 'display: flex']); ?>
                <input type="text" class="form-control float-right" name="search" placeholder="Поиск по названию компании..."
                       aria-label="Search"
                value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit">Найти</button>
                </form>
            </div>
        </div>
        <div class="plushkiStyle mb-3">
            <button type="button" class="btn btn-primary" disabled="disabled">За последние 30 дней потрачено:<?= esc($carwashSum['washCost']); ?>₽</button>
            <button type="button" class="btn btn-primary" disabled="disabled">Моек за последние 30 дней:<?= esc($carwashCount['washCost']); ?></button>
            <button type="button" class="btn btn-primary" disabled="disabled">В среднем потрачено:<?php if($carwashSum['washCost']==0) echo "0"; else { echo ($carwashSum['washCost']/$carwashCount['washCost']); }?> ₽</button>

        </div>
        <?php if (!empty($carwash) && is_array($carwash)) : ?>
            <table class="resp-tab mb-2 text-center">
                <thead>
                <tr>
                    <th>Тип мойки</th>
                    <th>Стоимость мойки</th>
                    <th>Дата</th>
                    <th>Компания</th>
                    <th>Автомобиль</th>
                    <th>Отзыв после мойки</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($carwash as $item): ?>
                    <tr>
                        <td><span>Тип мойки</span><?= esc($item['typeOfWash']); ?></td>
                        <td><span>Стоимость мойки</span><?= esc($item['washCost']); ?></td>
                        <td><span>Дата</span><?= esc($item['date']); ?></td>
                        <td><span>Компания</span><?= esc($item['washcompany']); ?></td>
                        <td><span>Автомобиль</span><?= esc($item['brand']); ?> <?= esc($item['model']); ?></td>
                        <td><span>Отзыв после мойки</span><?= esc($item['washText']); ?></td>
                        <td><span>Действия</span>
                            <div class="row d-flex justify-content-center">
                                <div class="mr-1">
                                    <a href="<?= base_url() ?>/carwash/edit/<?= esc($item['id']); ?>"
                                       class="btn btn-warning btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Редактировать">
                                        <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span>
                                    </a>
                                </div>
                                <div>
                                    <a href="<?= base_url() ?>/carwash/delete/<?= esc($item['id']); ?>"
                                       class="btn btn-danger btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Удалить">
                                        <span class="iconify" data-icon="ant-design:delete-filled" data-inline="false"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="text-center">
                <a class="btn btn-outline-primary" href="<?= base_url() ?>/carwash/create">
                    Добавить запись
                </a>
            </div>
        <?php else : ?>
            <div class="text-center">
                <p>Записи не найдены </p>
                <a class="btn btn-outline-danger btn-lg" href="<?= base_url() ?>/carwash/create">
                    Создать запись
                </a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>