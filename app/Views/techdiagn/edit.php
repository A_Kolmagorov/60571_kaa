<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('techdiagn/update'); ?>
        <h2>Редактирование записи о тех.осмотре:</h2>
        <input type="hidden" name="id" value="<?= $techdiagn["id"] ?>">
        <div class="form-group">
            <label for="cost">Стоимость тех.осмотра:</label>
            <input type="number" min="1" step="0.1" class="form-control <?= ($validation->hasError('cost')) ? 'is-invalid' : ''; ?>"
                   name="cost"
                   id="cost"
                   value="<?=$techdiagn['cost']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('brand') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="mileage">Пробег:</label>
            <input type="number" min="1" step="1" class="form-control <?= ($validation->hasError('mileage')) ? 'is-invalid' : ''; ?>"
                   name="mileage"
                   id="mileage"
                   value="<?= $techdiagn['mileage']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('model') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="techdiagnText">Текст:</label>
            <textarea rows="5" cols="60"  placeholder="Введите текст выполненных работ..." class="form-control <?= ($validation->hasError('techdiagnText')) ? 'is-invalid' : ''; ?>"
                      name="techdiagnText"
                      id="techdiagnText"><?= $techdiagn['techdiagnText']; ?></textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('techdiagnText') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="techdiagnDate">Дата прохождения:</label>
            <input type="date" class="form-control <?= ($validation->hasError('techdiagnDate')) ? 'is-invalid' : ''; ?>"
                   name="techdiagnDate"
                   id="techdiagnDate"
                   value="<?= $techdiagn['techdiagnDate']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('year') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="picture_url">Изображение:</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
                   name="picture_url" id="picture_url">
            <div class="invalid-feedback">
                <?= $validation->getError('picture_url') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
<?php
