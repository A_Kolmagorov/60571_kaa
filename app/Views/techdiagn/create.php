<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('techdiagn/store'); ?>
    <h2>Добавление записи о тех.осмотре:</h2>
    <div class="form-group">
        <label for="auto_id">Авто:</label>
        <select class="form-control <?= ($validation->hasError('auto_id')) ? 'is-invalid' : ''; ?>" name='auto_id'
                onChange=""
                id="auto_id"
        >
            <option value="">Выберите авто</option>
            <?php foreach ($auto as $item): ?>
                <option value=<?= esc($item['id']); ?>>
                    <?= esc($item['brand']); ?> <?= esc($item['model']); ?>
                </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('auto_id') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="cost">Стоимость тех.осмотра:</label>
        <input type="number" min="1" step="0,1" class="form-control <?= ($validation->hasError('cost')) ? 'is-invalid' : ''; ?>"
               name="cost"
               id="cost"
               value="<?= old('cost'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('cost') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="mileage">Пробег:</label>
        <input type="number" min="1" step="1" class="form-control <?= ($validation->hasError('mileage')) ? 'is-invalid' : ''; ?>"
               name="mileage"
               id="mileage"
               value="<?= old('mileage'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('mileage') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="techdiagnText">Текст:</label>
        <textarea rows="5" cols="60"  placeholder="Выполненная работа..." class="form-control <?= ($validation->hasError('techdiagnText')) ? 'is-invalid' : ''; ?>"
                  name="techdiagnText"
                  id="techdiagnText"></textarea>
        <div class="invalid-feedback">
            <?= $validation->getError('noteText') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="techdiagnDate">Дата:</label>
        <input type="date" class="form-control <?= ($validation->hasError('techdiagnDate')) ? 'is-invalid' : ''; ?>"
               name="techdiagnDate"
               id="techdiagnDate"
               value="<?= old('techdiagnDate'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('techdiagnDate') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="picture_url">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
               name="picture_url" id="picture_url">
        <div class="invalid-feedback">
            <?= $validation->getError('picture_url') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Добавить</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
