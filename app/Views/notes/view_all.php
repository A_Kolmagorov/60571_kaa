<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php if (!empty($auto) && is_array($auto)) : ?>
        <h2>Сервис:</h2>
        <div class="d-flex justify-content-between mb-4">
            <?= $pager->links('group1', "my_page") ?>
            <?= form_open('auto', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit">На странице</button>
            </form>
            <?= form_open('auto', ['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Модель или бренд авто"
                   aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit">Найти</button>
            </form>
        </div>
        <div class="row text-center mb-3">
            <div class="col-1">Фото</div>
            <div class="col-2">Модель</div>
            <div class="col-2">Бренд</div>
            <div class="col-1">Год</div>
            <div class="col-2">Топливо</div>
        </div>
        <?php foreach ($auto as $item): ?>
            <div class="row align-items-center text-center py-3 mb-3"
                 style="border: 2px solid grey; border-radius: 10px;">
                <div class="col-1">
                    <?php if (is_null($item['picture_url'])) : ?>
                        <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.ytimg.com%2Fvi%2FyjEeya5aaf8%2Fmaxresdefault.jpg&f=1&nofb=1"
                             class="w-100" alt="">
                    <?php else:?>
                        <img src="<?= esc($item['picture_url']); ?>" class="w-100" alt="">
                    <?php endif ?>
                </div>
                <div class="col-2">
                    <?= esc($item['model']); ?>
                </div>
                <div class="col-2">
                    <?= esc($item['brand']); ?>
                </div>
                <div class="col-1">
                    <?= esc($item['year']); ?>
                </div>
                <div class="col-2">
                    <?php if (!empty($item['fuel'])): ?>
                        <?= esc($item['fuel']); ?>
                    <?php else : ?>
                        Нет данных
                    <?php endif ?>
                </div>
                <div class="col-4 d-flex align-items-center justify-content-end">
                    <div>
                        <a href="<?= base_url() ?>/auto/view/<?= esc($item['id']); ?>"
                           class="btn btn-primary btn-sm">
                            <span class="iconify" data-icon="bi:card-list" data-inline="false"></span>
                        </a>
                    </div>
                    <div>
                        <a href="<?= base_url() ?>/auto/edit/<?= esc($item['id']); ?>"
                           class="btn btn-warning btn-sm">
                            <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span>
                        </a>
                    </div>
                    <div>
                        <a href="<?= base_url() ?>/auto/delete/<?= esc($item['id']); ?>"
                           class="btn btn-danger btn-sm">
                            <span class="iconify" data-icon="ant-design:delete-filled" data-inline="false"></span>
                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <div class="text-center">
            <p>Записи не найдены </p>
            <a class="btn btn-outline-danger btn-lg" href="<?= base_url() ?>/auto/create">
                Создать запись
            </a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
