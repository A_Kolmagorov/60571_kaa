<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('notes/update'); ?>
        <h2>Редактирование заметки:</h2>
        <input type="hidden" name="id" value="<?= $notes["id"] ?>">
        <div class="form-group">
            <label for="noteText">Текст:</label>
            <textarea rows="5" cols="60"  placeholder="Введите текст выполненных работ..." class="form-control <?= ($validation->hasError('noteText')) ? 'is-invalid' : ''; ?>"
                      name="noteText"
                      id="noteText"><?= $notes['noteText']; ?></textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('noteText') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="date">Дата заметки:</label>
            <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
                   name="date"
                   id="date"
                   value="<?= $notes['date']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
<?php
