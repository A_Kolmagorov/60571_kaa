<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Заметки:</h2>
        <div class="d-flex justify-content-between mb-4">
            <?= $pager->links('group1', "my_page") ?>
            <?= form_open('notes', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit">ОК</button>
            </form>
            <?= form_open('notes', ['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Поиск по модели авто..."
                   aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit">Найти</button>
            </form>
        </div>
        <?php if (!empty($notes) && is_array($notes)) : ?>
            <table class="resp-tab mb-2 text-center">
                <thead>
                <tr>
                    <th>Заметка</th>
                    <th>Дата</th>
                    <th>Автомобиль</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($notes as $item): ?>
                    <tr>
                        <td><span>Заметка</span><?= esc($item['noteText']); ?></td>
                        <td><span>Дата</span><?= esc($item['date']); ?></td>
                        <td><span>Автомобиль</span><?= esc($item['brand']); ?> <?= esc($item['model']); ?></td>
                        <td><span>Действия</span>
                            <div class="row d-flex justify-content-center">
                                <div class="mr-1">
                                    <a href="<?= base_url() ?>/notes/edit/<?= esc($item['id']); ?>"
                                       class="btn btn-warning btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Редактировать">
                                        <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span>
                                    </a>
                                </div>
                                <div>
                                    <a href="<?= base_url() ?>/notes/delete/<?= esc($item['id']); ?>"
                                       class="btn btn-danger btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Удалить">
                                        <span class="iconify" data-icon="ant-design:delete-filled" data-inline="false"></span>
                                    </a>
                                </div>
                           </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="text-center">
                <a class="btn btn-outline-primary" href="<?= base_url() ?>/notes/create">
                    Добавить запись
                </a>
            </div>
        <?php else : ?>
            <div class="text-center">
                <p>Записи не найдены </p>
                <a class="btn btn-outline-danger btn-lg" href="<?= base_url() ?>/notes/create">
                    Создать запись
                </a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>