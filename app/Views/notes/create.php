<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('notes/store'); ?>
    <h2>Добавление заметок:</h2>
    <div class="form-group">
        <label for="auto_id">Авто:</label>
        <select class="form-control <?= ($validation->hasError('auto_id')) ? 'is-invalid' : ''; ?>" name='auto_id'
                onChange=""
                id="auto_id"
        >
            <option value="">Выберите авто</option>
            <?php foreach ($auto as $item): ?>
                <option value=<?= esc($item['id']); ?>>
                    <?= esc($item['brand']); ?> <?= esc($item['model']); ?>
                </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('auto_id') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="noteText">Текст:</label>
        <textarea rows="5" cols="60"  placeholder="Введите текст заметки..." class="form-control <?= ($validation->hasError('noteText')) ? 'is-invalid' : ''; ?>"
                  name="noteText"
                  id="noteText"></textarea>
        <div class="invalid-feedback">
            <?= $validation->getError('noteText') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="date">Дата создания заметки:</label>
        <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
               name="date"
               id="date"
               value="<?= old('date'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('date') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Добавить</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
