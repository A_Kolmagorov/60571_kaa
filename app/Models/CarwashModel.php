<?php


namespace App\Models;


use CodeIgniter\Model;

class CarwashModel extends Model
{
    protected $table = 'carwash'; //таблица, связанная с моделью
    protected $allowedFields = ['typeOfWash', 'washCost', 'date', 'user_id','auto_id','id','carwash.id','washText','washcompany'];

    public function getCarwash($id = null, $user_id = null, $search = '')
    {
        $builder = $this->select(['typeOfWash', 'washCost', 'date', 'carwash.user_id','carwash.auto_id','carwash.id','brand','model','washcompany','washText'])->join('auto','carwash.auto_id = auto.id')->where(['carwash.user_id' => $user_id]);
        if(!$search=='') $builder = $builder->like('washcompany', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['carwash.id' => $id])->first();
        }
        return $builder;
    }
    public function getCarwashSum($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('washCost')->where(['carwash.user_id' => $user_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getCarwashCount($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectCount('washCost')->where(['carwash.user_id' => $user_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getCarwashSumMonth($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('washCost')->where(['carwash.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getCarwashSumYear($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 years"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('washCost')->where(['carwash.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getCarwashSum3Year($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-3 years"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('washCost')->where(['carwash.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
}