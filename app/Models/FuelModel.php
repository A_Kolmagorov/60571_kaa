<?php


namespace App\Models;


use CodeIgniter\Model;

class FuelModel extends Model
{
    protected $table = 'fuel'; //таблица, связанная с моделью
    protected $allowedFields = ['fuelType', 'liters', 'literCost','sum','auto_id', 'user_id','id','fuel.id','date'];

    public function getFuel($id = null, $user_id = null, $search = '')
    {
        $builder = $this->select(['fuelType', 'liters', 'literCost','sum','fuel.auto_id', 'fuel.user_id','fuel.id','brand','model','date'])->join('auto','fuel.auto_id = auto.id')->where(['fuel.user_id' => $user_id]);
        if(!$search=='') $builder = $builder->like('model', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['fuel.id' => $id])->first();
        }
        return $builder;
    }
    public function getFuelSum($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('sum')->where(['fuel.user_id' => $user_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getFuelCount($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectCount('sum')->where(['fuel.user_id' => $user_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getFuelSumMonth($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('sum')->where(['fuel.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getFuelSumYear($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 years"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('sum')->where(['fuel.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getFuelSum3Year($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-3 years"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('sum')->where(['fuel.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
}