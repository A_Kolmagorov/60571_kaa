<?php


namespace App\Models;


use CodeIgniter\Model;

class AutoModel extends Model
{
    protected $table = 'auto'; //таблица, связанная с моделью
    protected $allowedFields = ['brand', 'model', 'year', 'fuel', 'id', 'picture_url', 'user_id'];

    public function getAutoArray($id = null, $search = '')
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
    public function getAuto($id = null, $user_id = null, $search = '')
    {
        $builder = $this->select('*')->where(['user_id' => $user_id]);
        if(!$search=='') $builder = $builder->like('model', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['id' => $id])->first();
        }
        return $builder;
    }
    public function getUserAuto($user_id = null)
    {
        return $this->select('*')->where(['user_id' => $user_id])->findAll();
    }
    public function getUserFirstAuto($user_id = null)
    {
        return $this->select('*')->where(['user_id' => $user_id])->first();
    }
    public function getAutoInfo($id = null,$user_id = null)
    {
        $builder=$this->select('*')->where(['user_id' => $user_id]);
        return $builder->where(['id' => $id])->first();
    }
}
