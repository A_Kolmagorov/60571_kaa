<?php


namespace App\Models;


use CodeIgniter\Database\Exceptions\DataException;
use CodeIgniter\Model;

class NotesModel extends Model
{
    protected $table = 'notes'; //таблица, связанная с моделью
    protected $allowedFields = ['date', 'noteText','auto_id', 'user_id','id','notes.id'];

    public function getNotes($id = null, $user_id = null, $search = '')
    {
        $builder = $this->select(['date', 'noteText','notes.auto_id', 'notes.user_id','notes.id','brand','model'])->join('auto','notes.auto_id = auto.id')->where(['notes.user_id' => $user_id]);
        if(!$search=='') $builder = $builder->like('model', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['notes.id' => $id])->first();
        }
        return $builder;
    }
    public function getNotesForEdit($id = null)
    {
        $this->select('*')->where(['id'=> $id])->first();
    }
}