<?php


namespace App\Models;


use CodeIgniter\Model;

class InsuranceModel extends Model
{
    protected $table = 'insurance'; //таблица, связанная с моделью
    protected $allowedFields = ['typeOfInsurance', 'mileage', 'cost','insuranceDate','auto_id', 'user_id','company','insuranceDuration','id','insurance.id','picture_url'];

    public function getInsurance($id = null, $user_id = null, $search = '')
    {
        $builder = $this->select(['insurance.picture_url','typeOfInsurance', 'mileage', 'cost','insuranceDate','insurance.auto_id', 'insurance.user_id','company','insuranceDuration','insurance.id','brand','model'])->join('auto','insurance.auto_id = auto.id')->where(['insurance.user_id' => $user_id]);
        if(!$search=='') $builder = $builder->like('model', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['insurance.id' => $id])->first();
        }
        return $builder;
    }
    public function getInsuranceSum($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('cost')->where(['insurance.user_id' => $user_id])->where("insuranceDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getInsuranceCount($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectCount('cost')->where(['insurance.user_id' => $user_id])->where("insuranceDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getInsuranceSumMonth($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('cost')->where(['auto_id' => $auto_id])->where("insuranceDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getInsuranceSumYear($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 years"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('cost')->where(['auto_id' => $auto_id])->where("insuranceDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getInsuranceSum3Year($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-3 years"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('cost')->where(['auto_id' => $auto_id])->where("insuranceDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
}