<?php namespace App\Models;
use CodeIgniter\Database\Exceptions\DataException;
use CodeIgniter\Model;
class ServiceModel extends Model
{
    protected $table = 'service'; //таблица, связанная с моделью
    protected $allowedFields = ['date', 'detcost', 'typeofwork', 'workcost', 'servicename', 'mileage', 'auto_id', 'id',
        'picture_url','service.id','user_id'];
//    public function getCar($id = null, $search = '')
//    {
//        if (!isset($id)) {
//            return $this->select('*')->join('auto','service.auto_id = auto.id')
//                ->like('typeofwork', $search,'both', null, true)
//                ->orlike('brand',$search,'both',null,true)->findAll();
//        }
//        return $this->where(['auto.id' => $id])->first();
//    }
    public function getServiceWithAuto($id = null,$user_id = null, $search = '')
    {
        $builder = $this->select(['date', 'detcost', 'typeofwork', 'workcost', 'servicename', 'mileage', 'auto_id',
            'service.picture_url','service.id','brand','model'])->join('auto','service.auto_id = auto.id')->where(['service.user_id' => $user_id]);
        if(!$search=='') $builder = $builder->like('typeofwork', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['service.id' => $id])->first();
        }
        return $builder;
    }
    public function getServiceForEdit($id = null)
    {
        if(!is_null($id)) return $this->select('*')->where(['id'=> $id])->first();
        else return null;
    }
    public function getServiceSum($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        $sumOfWorkcost=$this->selectSum('workcost')->where(['service.user_id' => $user_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
        $sumOfDetcost=$this->selectSum('detcost')->where(['service.user_id' => $user_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
        return $sumOfWorkcost+$sumOfDetcost;
    }
    public function getServiceCount($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectCount('workcost')->where(['service.user_id' => $user_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getServiceSumMonth($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        $detcost=$this->selectSum('detcost')->where(['service.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
        $workcost=$this->selectSum('workcost')->where(['service.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
        return $detcost+$workcost;
    }
    public function getServiceSumYear($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 years"));
        $maxvalue = date('Y-m-d');
        $detcost=$this->selectSum('detcost')->where(['service.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
        $workcost=$this->selectSum('workcost')->where(['service.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
        return $detcost+$workcost;
    }
    public function getServiceSum3Year($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-3 years"));
        $maxvalue = date('Y-m-d');
        $detcost=$this->selectSum('detcost')->where(['service.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
        $workcost=$this->selectSum('workcost')->where(['service.auto_id' => $auto_id])->where("date BETWEEN '$minvalue' AND '$maxvalue'")->first();
        return $detcost+$workcost;
    }
}