<?php


namespace App\Models;


use CodeIgniter\Model;

class TechdiagnModel extends Model
{
    protected $table = 'techdiagn'; //таблица, связанная с моделью
    protected $allowedFields = ['cost', 'mileage', 'techdiagnDate','auto_id', 'user_id','id','techdiagnText','techdiagn.id','picture_url'];

    public function getTechdiagn($id = null, $user_id = null, $search = '')
    {
        $builder = $this->select(['techdiagn.picture_url','cost', 'mileage', 'techdiagnDate','techdiagn.auto_id', 'techdiagn.user_id','techdiagn.id','techdiagnText','brand','model'])->join('auto','techdiagn.auto_id = auto.id')->where(['techdiagn.user_id' => $user_id]);
        if(!$search=='') $builder = $builder->like('model', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['techdiagn.id' => $id])->first();
        }
        return $builder;
    }
    public function getTechdiagnSum($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('cost')->where(['techdiagn.user_id' => $user_id])->where("techdiagnDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getTechdiagnCount($user_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectCount('cost')->where(['techdiagn.user_id' => $user_id])->where("techdiagnDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getTechdiagnSumMonth($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 months"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('cost')->where(['techdiagn.auto_id' => $auto_id])->where("techdiagnDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getTechdiagnSumYear($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-1 years"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('cost')->where(['techdiagn.auto_id' => $auto_id])->where("techdiagnDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
    public function getTechdiagnSum3Year($auto_id = null)
    {
        $minvalue = date("Y-m-d", strtotime("-3 years"));
        $maxvalue = date('Y-m-d');
        return $this->selectSum('cost')->where(['techdiagn.auto_id' => $auto_id])->where("techdiagnDate BETWEEN '$minvalue' AND '$maxvalue'")->first();
    }
}