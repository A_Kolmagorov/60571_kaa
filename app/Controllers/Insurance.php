<?php


namespace App\Controllers;

use App\Models\AutoModel;
use App\Models\InsuranceModel;
use App\Models\NotesModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;

class Insurance extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        } else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search'))) {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        } else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;
        helper(['form', 'url']);
        $model = new InsuranceModel();
        $data['insurance'] = $model->getInsurance(null, $this->ionAuth->getUserId(), $search)->paginate($per_page, 'group1');
        $data['insuranceSum'] = $model->getInsuranceSum($this->ionAuth->getUserId());
        $data['insuranceCount'] = $model->getInsuranceCount($this->ionAuth->getUserId());
        $data['pager'] = $model->pager;
        echo view('insurance/view_all_with_car', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $modelInsurance = new InsuranceModel();
        $data ['insurance'] = $modelInsurance->getInsurance($id,$this->ionAuth->getUserId());
        $modelAuto = new AutoModel();
        $data ['auto'] = $modelAuto->getAutoInfo($data ['insurance']['auto_id'],$this->ionAuth->getUserId());
        $this->view = view('insurance/view', $this->withIon($data));
        echo $this->view;
    }
    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new AutoModel();
        $data ['auto'] = $model->getUserAuto($this->ionAuth->getUserId());
        $data ['validation'] = \Config\Services::validation();
        echo view('insurance/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'auto_id' => 'required',
                'typeOfInsurance' => 'required',
                'mileage' => 'required',
                'cost' => 'required',
                'insuranceDate' => 'required',
                'insuranceDuration' => 'required',
                'company' => 'required',
                'picture_url' => 'is_image[picture_url]|max_size[picture_url,2048]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture_url');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new InsuranceModel();
            $data = [
                'user_id' => $this->ionAuth->getUserId(),
                'auto_id' => $this->request->getPost('auto_id'),
                'typeOfInsurance' => $this->request->getPost('typeOfInsurance'),
                'insuranceDuration' =>$this->request->getPost('insuranceDuration'),
                'company' =>$this->request->getPost('company'),
                'mileage' => $this->request->getPost('mileage'),
                'cost' => $this->request->getPost('cost'),
                'insuranceDate' => $this->request->getPost('insuranceDate'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Car.insurance_create_success'));
            return redirect()->to('/insurance');
        } else {
            return redirect()->to('/insurance/create')->withInput();
        }
    }
    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $modelInsurance = new InsuranceModel();
        $data['insurance'] = $modelInsurance->getInsurance($id, $this->ionAuth->getUserId(), '');
        $data ['validation'] = \Config\Services::validation();
        echo view('insurance/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/insurance/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'mileage' => 'required',
                'cost' => 'required',
                'insuranceDate' => 'required',
                'insuranceDuration' => 'required',
                'company' => 'required',
                'picture_url' => 'is_image[picture_url]|max_size[picture_url,1024]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture_url');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new InsuranceModel();
            session()->setFlashdata('message', lang('Car.insurance_update_success'));
            $data = [
                'id' => $this->request->getPost('id'),
                'mileage' => $this->request->getPost('mileage'),
                'cost' => $this->request->getPost('cost'),
                'insuranceDate' => $this->request->getPost('insuranceDate'),
                'insuranceDuration' => $this->request->getPost('insuranceDuration'),
                'company' => $this->request->getPost('company'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            return redirect()->to('/insurance');
        } else {
            return redirect()->to('/insurance/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id): \CodeIgniter\HTTP\RedirectResponse
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new InsuranceModel();
        $model->delete($id);
        return redirect()->to('/insurance');
    }
}