<?php


namespace App\Controllers;

use App\Models\AutoModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;

class Auto extends BaseController
{
    public function index() //Обображение всех записей из бд.
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        } else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search'))) {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        } else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;
        helper(['form', 'url']);
        $model = new AutoModel();
        $data['auto'] = $model->getAuto(null, $this->ionAuth->getUserId(), $search)->paginate($per_page, 'group1');
        $data['pager'] = $model->pager;
        echo view('auto/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new AutoModel();
        $data ['auto'] = $model->getAutoArray($id);
        $this->view = view('auto/view', $this->withIon($data));
        echo $this->view;
    }
    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new AutoModel();
        $data ['auto'] = $model->getAuto();
        $data ['validation'] = \Config\Services::validation();
        echo view('auto/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
//                'id'  => 'required',
                'brand' => 'required',
                'model' => 'required',
                'year' => 'required',
                'fuel' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }

            $model = new AutoModel();
            $data = [
                'user_id' => $this->ionAuth->getUserId(),
                'brand' => $this->request->getPost('brand'),
                'model' => $this->request->getPost('model'),
                'year' => $this->request->getPost('year'),
                'fuel' => $this->request->getPost('fuel'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Car.car_create_success'));
            return redirect()->to('/auto');
        } else {
            return redirect()->to('/auto/create')->withInput();
        }
    }
    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new AutoModel();
        $data ['auto'] = $model->getAutoArray($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('auto/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/auto/edit/' . $this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'brand' => 'required',
                'model' => 'required',
                'year' => 'required',
                'fuel' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new AutoModel();

            session()->setFlashdata('message', lang('Car.car_update_success'));
            $data = [
                'id' => $this->request->getPost('id'),
                'brand' => $this->request->getPost('brand'),
                'model' => $this->request->getPost('model'),
                'year' => $this->request->getPost('year'),
                'fuel' => $this->request->getPost('fuel'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            return redirect()->to('/auto');
        } else {
            return redirect()->to('/auto/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new AutoModel();
        $model->delete($id);
        return redirect()->to('/auto');
    }
}