<?php


namespace App\Controllers;

use App\Models\AutoModel;
use App\Models\CarwashModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;

class Carwash extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        } else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search'))) {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        } else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;
        helper(['form', 'url']);
        $model = new CarwashModel();
        $data['carwash'] = $model->getCarwash(null, $this->ionAuth->getUserId(), $search)->paginate($per_page, 'group1');
        $data['carwashSum'] = $model->getCarwashSum($this->ionAuth->getUserId());
        $data['carwashCount'] = $model->getCarwashCount($this->ionAuth->getUserId());
        $data['pager'] = $model->pager;
        echo view('carwash/view_all_with_car', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new CarwashModel();
        $data ['carwash'] = $model->getCarwash($id,$this->ionAuth->getUserId());
        $this->view = view('carwash/view', $this->withIon($data));
        echo $this->view;
    }
    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new AutoModel();
        $data ['auto'] = $model->getUserAuto($this->ionAuth->getUserId());
        $data ['validation'] = \Config\Services::validation();
        echo view('carwash/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'auto_id' => 'required',
                'typeOfWash' => 'required',
                'washText' => 'required',
                'washcompany' => 'required',
                'washCost' => 'required',
                'date' => 'required',
            ])) {
            $model = new CarwashModel();
            $data = [
                'user_id' => $this->ionAuth->getUserId(),
                'auto_id' => $this->request->getPost('auto_id'),
                'typeOfWash' => $this->request->getPost('typeOfWash'),
                'washcompany' => $this->request->getPost('washcompany'),
                'washText' => $this->request->getPost('washText'),
                'washCost' => $this->request->getPost('washCost'),
                'date' => $this->request->getPost('date'),
            ];
            $model->save($data);
            session()->setFlashdata('message', lang('Car.wash_create_success'));
            return redirect()->to('/carwash');
        } else {
            return redirect()->to('/carwash/create')->withInput();
        }
    }
    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new CarwashModel();
        $data['carwash'] = $model->getCarwash($id, $this->ionAuth->getUserId(), '');
        $data ['validation'] = \Config\Services::validation();
        echo view('carwash/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/carwash/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'washCost' => 'required',
                'date' => 'required',
                'washText' => 'required',
                'washcompany' => 'required',
            ])) {
            $model = new CarwashModel();

            session()->setFlashdata('message', lang('Car.carwash_update_success'));
            $data = [
                'id' => $this->request->getPost('id'),
                'washCost' => $this->request->getPost('washCost'),
                'date' => $this->request->getPost('date'),
                'washcompany' => $this->request->getPost('washcompany'),
                'washText' => $this->request->getPost('washText'),
            ];
            $model->save($data);
            return redirect()->to('/carwash');
        } else {
            return redirect()->to('/carwash/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new CarwashModel();
        $model->delete($id);
        return redirect()->to('/carwash');
    }
}