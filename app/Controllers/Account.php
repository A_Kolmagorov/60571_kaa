<?php


namespace App\Controllers;


use App\Models\UserModel;
use Aws\S3\S3Client;

class Account extends BaseController
{
    public function index()
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('/', $this->withIon($data));
    }

    public function editAvatar($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new UserModel();

        helper(['form']);
        $data ['order'] = $model->getUser($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('/', $this->withIon($data));
    }

    public function setAvatar()
    {
//        $id = $this->input->post('id');
//        var_dump($id);
//        $model = new UserModel();
//        $data ['user'] = $model->getUser($id);

        helper(['form', 'url']);
        if ($this->request->getMethod() === 'post' && $this->validate([
                'picture_url' => 'max_size[picture_url,1024]'
            ])) {

            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture_url');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

                $model = new UserModel();
                $data = [
                    'id' => $this->request->getPost('id'),
                ];
                if (!is_null($insert))
                    $data['picture_url'] = $insert['ObjectURL'];
                $model->save($data);
            }
                return redirect()->to('/');

        }
    }
}