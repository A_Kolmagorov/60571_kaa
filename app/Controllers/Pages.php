<?php
namespace App\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\Model;
use App\Models\AutoModel;
use App\Models\CarwashModel;
use App\Models\FuelModel;
use App\Models\InsuranceModel;
use App\Models\ServiceModel;
use App\Models\TechdiagnModel;

class Pages extends BaseController
{

    public function index()
    {

        return view('welcome_message', $this->withIon());
    }
    public function view($page = 'main')
    {
        if ($this->ionAuth->loggedIn()) {
            //Обработка запроса на статистику авто
            $model = new AutoModel();
            if (!is_null($this->request->getPost('auto_id'))) {
                session()->setFlashdata('auto_id', $this->request->getPost('auto_id'));
                $auto_id = $this->request->getPost('auto_id');
                $data['auto_id']['id'] = $auto_id;
            } else {
                $data['auto_id'] = $model->getUserFirstAuto($this->ionAuth->getUserId());
            }
            helper(['form', 'url']);
            $data['auto'] = $model->getUserAuto($this->ionAuth->getUserId());
            //сбор сумм по категориями для диаграмм
            $modelCarwash= new CarwashModel();
            $data['carwashMonth'] = $modelCarwash->getCarwashSumMonth($data['auto_id']['id']);
            $data['carwashYear'] = $modelCarwash->getCarwashSumYear($data['auto_id']['id']);
            $data['carwash3Year'] = $modelCarwash->getCarwashSum3Year($data['auto_id']['id']);
            $modelFuel= new FuelModel();
            $data['fuelMonth'] = $modelFuel->getFuelSumMonth($data['auto_id']['id']);
            $data['fuelYear'] = $modelFuel->getFuelSumYear($data['auto_id']['id']);
            $data['fuel3Year'] = $modelFuel->getFuelSum3Year($data['auto_id']['id']);
            $modelInsurance= new InsuranceModel();
            $data['insuranceMonth'] = $modelInsurance->getInsuranceSumMonth($data['auto_id']['id']);
            $data['insuranceYear'] = $modelInsurance->getInsuranceSumYear($data['auto_id']['id']);
            $data['insurance3Year'] = $modelInsurance->getInsuranceSum3Year($data['auto_id']['id']);
            $modelService= new ServiceModel();
            $data['serviceMonth'] = $modelService->getServiceSumMonth($data['auto_id']['id']);
            $data['serviceYear'] = $modelService->getServiceSumYear($data['auto_id']['id']);
            $data['service3Year'] = $modelService->getServiceSum3Year($data['auto_id']['id']);
            $modelTechdiagn= new TechdiagnModel();
            $data['techdiagnMonth'] = $modelTechdiagn->getTechdiagnSumMonth($data['auto_id']['id']);
            $data['techdiagnYear'] = $modelTechdiagn->getTechdiagnSumYear($data['auto_id']['id']);
            $data['techdiagn3Year'] = $modelTechdiagn->getTechdiagnSum3Year($data['auto_id']['id']);
            echo view('pages/loggedin', $this->withIon($data));
        }
        else if ( ! is_file(APPPATH.'/Views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        else echo view('pages/'.$page, $this->withIon());
    }

}
