<?php namespace App\Controllers;

use App\Models\AutoModel;
use App\Models\ServiceModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;

class Service extends BaseController
{
    private $view;

    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        } else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search'))) {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        } else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;
        helper(['form', 'url']);
        $model = new ServiceModel();
        $data['service'] = $model->getServiceWithAuto(null, $this->ionAuth->getUserId(), $search)->paginate($per_page, 'group1');
        $data['serviceSum'] = $model->getServiceSum($this->ionAuth->getUserId());
        $data['serviceCount'] = $model->getServiceCount($this->ionAuth->getUserId());
        $data['pager'] = $model->pager;
        echo view('service/view_all_with_auto', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ServiceModel();
        $data ['service'] = $model->getServiceWithAuto($id,$this->ionAuth->getUserId());
        $modelAuto = new AutoModel();
        $data ['auto'] = $modelAuto->getAutoInfo($data ['service']['auto_id'],$this->ionAuth->getUserId());
        $this->view = view('service/view', $this->withIon($data));
        echo $this->view;
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new AutoModel();
        $data ['auto'] = $model->getUserAuto($this->ionAuth->getUserId());
        $data ['validation'] = \Config\Services::validation();
        echo view('service/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
//                'id'  => 'required',
                'date' => 'required',
                'detcost' => 'required',
                'typeofwork' => 'required',
                'workcost' => 'required',
                'servicename' => 'required',
                'mileage' => 'required',
                'picture_url' => 'is_image[picture_url]|max_size[picture_url,1024]',
                'auto_id' => 'required',
            ])) {

            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture_url');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }


            $model = new ServiceModel();
            $data = [
                'user_id' => $this->ionAuth->getUserId(),
                'date' => $this->request->getPost('date'),
                'detcost' => $this->request->getPost('detcost'),
                'typeofwork' => $this->request->getPost('typeofwork'),
                'workcost' => $this->request->getPost('workcost'),
                'servicename' => $this->request->getPost('servicename'),
                'mileage' => $this->request->getPost('mileage'),
                'auto_id' => $this->request->getPost('auto_id'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Car.service_create_success'));
            return redirect()->to('/service');
        } else {
            return redirect()->to('/service/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $modelService = new ServiceModel();
        $modeAuto = new AutoModel();
        helper(['form']);
        $data ['service'] = $modelService->getServiceForEdit($id);
        $data ['auto'] = $modeAuto->getUserAuto($this->ionAuth->getUserId());
        $data ['validation'] = \Config\Services::validation();
        echo view('service/edit', $this->withIon($data));
    }

    /**
     * @throws \ReflectionException
     */
    public function update()
    {
        helper(['form', 'url']);
        echo '/service/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'date' => 'required',
                'detcost' => 'required',
                'typeofwork' => 'required',
                'workcost' => 'required',
                'servicename' => 'required',
                'mileage' => 'required',
                'picture_url' => 'is_image[picture_url]|max_size[picture_url,1024]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture_url');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }

            $model = new ServiceModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'date' => $this->request->getPost('date'),
                'detcost' => $this->request->getPost('detcost'),
                'typeofwork' => $this->request->getPost('typeofwork'),
                'workcost' => $this->request->getPost('workcost'),
                'servicename' => $this->request->getPost('servicename'),
                'mileage' => $this->request->getPost('mileage'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Car.service_update_success'));

            return redirect()->to('/service');
        } else {
            return redirect()->to('/service/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ServiceModel();
        $model->delete($id);
        return redirect()->to('/service');
    }

//    public function viewAllWithAuto()
//    {
//        if (!$this->ionAuth->loggedIn()) {
//            return redirect()->to('/auth/login');
//        }
//        //Подготовка значения количества элементов выводимых на одной странице
//        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
//        {
//            //сохранение кол-ва страниц в переменной сессии
//            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
//            $per_page = $this->request->getPost('per_page');
//        } else {
//            $per_page = session()->getFlashdata('per_page');
//            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
//            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
//        }
//        $data['per_page'] = $per_page;
//        //Обработка запроса на поиск
//        if (!is_null($this->request->getPost('search'))) {
//            session()->setFlashdata('search', $this->request->getPost('search'));
//            $search = $this->request->getPost('search');
//        } else {
//            $search = session()->getFlashdata('search');
//            session()->setFlashdata('search', $search);
//            if (is_null($search)) $search = '';
//        }
//        $data['search'] = $search;
//        helper(['form', 'url']);
//        $model = new ServiceModel();
//        $data['service'] = $model->getServiceWithAuto(null, $search)->paginate($per_page, 'group1');
//        $data['pager'] = $model->pager;
//        echo view('service/view_all_with_auto', $this->withIon($data));
//    }
}