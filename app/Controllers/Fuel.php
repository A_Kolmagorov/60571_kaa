<?php


namespace App\Controllers;

use App\Models\AutoModel;
use App\Models\FuelModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;

class Fuel extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        } else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search'))) {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        } else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;
        helper(['form', 'url']);
        $model = new FuelModel();
        $data['fuel'] = $model->getFuel(null, $this->ionAuth->getUserId(), $search)->paginate($per_page, 'group1');
        $data['fuelSum'] = $model->getFuelSum($this->ionAuth->getUserId());
        $data['fuelCount'] = $model->getFuelCount($this->ionAuth->getUserId());
        $data['pager'] = $model->pager;
        echo view('fuel/view_all_with_car', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new FuelModel();
        $data ['fuel'] = $model->getFuel($id);
        $this->view = view('fuel/view', $this->withIon($data));
        echo $this->view;
    }
    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new AutoModel();
        $data ['auto'] = $model->getUserAuto($this->ionAuth->getUserId());
        $data ['validation'] = \Config\Services::validation();
        echo view('fuel/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'date' => 'required',
                'auto_id' => 'required',
                'liters' => 'required',
                'literCost' => 'required',
                'fuelType' => 'required',
            ])) {
            $model = new FuelModel();
            $data = [
                'user_id' => $this->ionAuth->getUserId(),
                'auto_id' => $this->request->getPost('auto_id'),
                'date' => $this->request->getPost('date'),
                'fuelType' => $this->request->getPost('fuelType'),
                'liters' => $this->request->getPost('liters'),
                'literCost' => $this->request->getPost('literCost'),
                'sum' => $this->request->getPost('liters')*$this->request->getPost('literCost'),
            ];
            $model->save($data);
            session()->setFlashdata('message', lang('Car.fuel_create_success'));
            return redirect()->to('/fuel');
        } else {
            return redirect()->to('/fuel/create')->withInput();
        }
    }
    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new FuelModel();
        $data['fuel'] = $model->getFuel($id, $this->ionAuth->getUserId(), '');
        $data ['validation'] = \Config\Services::validation();
        echo view('fuel/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/fuel/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'date' => 'required',
                'id' => 'required',
                'liters' => 'required',
                'literCost' => 'required',
            ])) {
            $model = new FuelModel();

            session()->setFlashdata('message', lang('Car.fuel_update_success'));
            $data = [
                'id' => $this->request->getPost('id'),
                'date' => $this->request->getPost('date'),
                'liters' => $this->request->getPost('liters'),
                'literCost' => $this->request->getPost('literCost'),
                'sum' => $this->request->getPost('liters')*$this->request->getPost('literCost'),
            ];
            $model->save($data);
            return redirect()->to('/fuel');
        } else {
            return redirect()->to('/fuel/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new FuelModel();
        $model->delete($id);
        return redirect()->to('/fuel');
    }
}