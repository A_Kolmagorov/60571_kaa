<?php

namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('278321777138-iqj1n26d9m6peftcc5m1qiq6m9bqrks5.apps.googleusercontent.com');
        $this->google_client->setClientSecret('4cDeOjicdJWTRxChFB1q1qz8');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }
}