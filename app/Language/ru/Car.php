<?php
return [
    'admin_permission_needed' => 'У вас нет доступа к этой странице.',
    'car_create_success' => 'Автомобиль успешно добавлен.',
    'wash_create_success' => 'Запись о мойке успешно добавлена.',
    'fuel_create_success' => 'Заправка топливом успешно добавлена.',
    'insurance_create_success' => 'Страхование успешно добавлено.',
    'techdiagn_create_success' => 'Запись о плановом тех.обслуживании успешно добавлена.',
    'service_create_success' => 'Запись о сервисе успешно добавлена.',
    'notes_create_success' => 'Заметка успешно добавлена.',
    'insurance_update_success' => 'Страхование успешно обновлено.',
    'car_update_success' => 'Данные автомобиля успешно обновлены.',
    'notes_update_success' => 'Заметка успешно обновлена.',
    'carwash_update_success' => 'Данные о мойке успешно обновлены.',
    'techdiagn_update_success' => 'Данные о ТО успешно обновлены.',
    'fuel_update_success' => 'Данные о заправке успешно обновлены.',
    'service_update_success' => 'Данные о сервисе успешно обновлены.',
];