(function () {
    console.log('App is working.');
    let btn = document.querySelector("button[name='btn-change-avatar']");
    let inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        let label = input.nextElementSibling,
            labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            let fileName = '',
                letterNumber = null,
                labelText = '';
            fileName = e.target.value;
            if (fileName) {
                for (let i = fileName.length - 1; fileName[i] !== '\\'; i--) {
                    letterNumber = i;
                }
                for (let i = letterNumber; i < fileName.length; i++) {
                    labelText = labelText + fileName[i];
                }
                label.innerHTML = labelText;
                btn.classList.remove('d-none');
            } else {
                label.innerHTML = labelVal;
                btn.classList.add('d-none');
            }
        });
    });
}());
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
})

document.querySelector('.third-button').addEventListener('click', function () {

    document.querySelector('.animated-icon3').classList.toggle('open');
});
$(document).ready(function() {
    $('#auto_id').on('change', function() {
        var $form = $(this).closest('form');
        $form.find('input[type=submit]').click();
    });
});